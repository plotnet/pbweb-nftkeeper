const pbw = require('pbwallet')

const keeper = require('./index')

const PBTLists = {
    owned: {}
}

function commit(method, updated){
    if(method=='setPBTLists'){
        console.log('setPBTLists', updated)
        for(var k in updated){
            PBTLists[k] = updated[k]
        }
    }else{
        console.log('commit: bad method', method)
    }
}

async function test(){
    if(process.argv.length!=3){
        console.log('usage', process.argv[0], process.argv[1], 'PRIVATE_KEY')
        process.exit(1)
    }
    const bsc = await pbw.connect_rpc(true, process.argv[2], 'https://data-seed-prebsc-1-s1.binance.org:8545/')
    await keeper.startKeeper(bsc, commit, PBTLists)
    const cnt = await keeper.preload(commit, PBTLists)
    console.log('preload remain data now', cnt, 'PBT owned')
}

test()
