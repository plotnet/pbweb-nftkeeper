const pbw = require('pbwallet')
const ethers = require('ethers')
// const tokenInfos = {}
var bsc = {}
var myList = {}
var marketList = {}
const ptInfos = {}
ptInfos[ethers.constants.AddressZero] = {
    symbol: "BNB",
    decimals: 18
}
async function ptInfo(ctraddr) {
    const ctr = pbw.erc20_contract(ctraddr)
    let info = {
        symbol: "invalid",
        decimals: -1
    }
    try {
        info.symbol = await ctr.symbol()
        info.decimals = await ctr.decimals()
    } catch (e) {
        console.log("read ctr", ctraddr, "err,maybe not ERC20")
        console.log('exception', e)
    }
    return info
}
async function tokenSymbol(ctraddr) {
    ctraddr = ethers.utils.getAddress(ctraddr)
    if (!(ctraddr in ptInfos) || ptInfos[ctraddr].decimals == -1) {
        ptInfos[ctraddr] = await ptInfo(ctraddr)
    }
    return ptInfos[ctraddr].symbol
}
async function getCoinTypes(pbtid) {
    const cointype = await bsc.ctrs.pbpuzzlehash.pbtCoinTypes(pbtid)
    console.log("getCoinTypes", cointype)
    return cointype
}

function copyObj(src, dest) {
    for (var k in src) {
        dest[k] = src[k]
    }
}

function fix_uri(uri) {
    if (uri.startsWith('ipfs:')) {
        return uri.replace('ipfs://', 'https://ipfs.io/ipfs/')
    } else {
        return uri
    }
}
async function formatToken(ctraddr, val) {
    ctraddr = ethers.utils.getAddress(ctraddr)
    if (!(ctraddr in ptInfos)) {
        ptInfos[ctraddr] = await ptInfo(ctraddr)
    }
    return ethers.utils.formatUnits(val, ptInfos[ctraddr].decimals)
}

async function parseToken(ctraddr, val) {
    ctraddr = ethers.utils.getAddress(ctraddr)
    if (!(ctraddr in ptInfos)) {
        ptInfos[ctraddr] = await ptInfo(ctraddr)
    }
    return ethers.utils.parseUnits(val, ptInfos[ctraddr].decimals)
}
async function nftBriefInfo(id) {
    const uri = await bsc.ctrs.pbt.tokenURI(Number(id))
    let meta = {}
    try {
        const metaData = await fetch(fix_uri(uri))
        const img = await metaData.json()
        meta = metaData
        meta['image'] = await fix_uri(String(img.image))
    } catch (e) {
        meta['image'] = "https://app.plotbridge.net/img/loading.png"
        console.log("brief-info err", e, meta)
    }

    const info = {
        id: Number(id),
        uri: uri,
        meta: meta,
    }
    return info
}

async function loadPbxs(pbtid) {
    const cointy = await getCoinTypes(pbtid)
    const pbxs = {}
    const cointyArr = cointy[0].concat(cointy[1])
    const atArr = Array.from(new Set(cointyArr))
    for (let i = 0; i < atArr.length; i++) {
        const ct = parseInt(atArr[i])
        const winfo = pbw.wcoin_info(ct)
        const xAddress = await bsc.ctrs.pbpuzzlehash.pbtPuzzleHash(pbtid, ct)
        const depAddr = window.ChiaUtils.puzzle_hash_to_address(String(xAddress[0]), winfo.prefix)
        const withAddr = window.ChiaUtils.puzzle_hash_to_address(String(xAddress[1]), winfo.prefix)
        const addrInfo = {
            depositAddr: String(depAddr),
            withdrawAddr: String(withAddr)
        }
        for (let k in addrInfo) {
            if (addrInfo[k].substr(3, 6) == "1qqqqq") {
                addrInfo[k] = false
            }
        }
        console.log("addrinfo", addrInfo)
        pbxs[String(ct)] = addrInfo
    }
    return pbxs
}
async function loadMarketinfo(id) {
    const sinfo = await bsc.ctrs.pbmarket.getSaleInfo(bsc.ctrs.pbt.address, Number(id))
    const info = {}
    info.priceToken = sinfo[0]
    info.ptName = await tokenSymbol(sinfo[0])
    info.price = await formatToken(sinfo[0], sinfo[1])
    info.desc = sinfo[2]
    info.seller = sinfo[3]
    info.owner = 'market'
    return info
}

function getmkLists(marketList, commit) {
    let msList = {}
    let mkList = {}
    for (let i in marketList) {
        if ('market' in marketList[i]) {
            if (marketList[i].market['price'] != '0.0') {
                mkList[i] = marketList[i]
            }
        }
    }
    for (let i in marketList) {
        if ('market' in marketList[i]) {
            if (marketList[i].market['seller'] == '-self') {
                msList[i] = marketList[i]
            }
        }
    }
    commit("setMarketlist", mkList)
    commit("setMySalelist", msList)
    console.log("marketList in listen", mkList, msList)
}

async function addToMyList(myList, id, commit) {
    const key = parseInt(id).toString()
    if (!(key in myList)) {
        const info = await nftBriefInfo(id)
        const pbxs = await loadPbxs(id)
        info['pbxs'] = pbxs
        myList[key] = info
        console.log("add mylist", myList)
        commit("setMylist", myList)
    } else {
        return false
    }
}

async function addToMarketList(marketList, id, commit) {
    const key = parseInt(id).toString()
    if (!(key in marketList)) {
        const info = await nftBriefInfo(id)
        const market = await loadMarketinfo(id)
        info['market'] = market
        marketList[key] = info
        console.log("addmklist", marketList)
        getmkLists(marketList, commit)
    } else {
        return false
    }
}

function deleteFromMkList(marketList, id, commit) {
    const key = parseInt(id).toString()
    if (key in marketList) {
        delete marketList[key]
        console.log("delet marketList", marketList)
        getmkLists(marketList, commit)
    } else {
        return false
    }
}

function deleteFromMyList(myList, id, commit) {
    const key = parseInt(id).toString()
    if (key in myList) {
        delete myList[key]
        console.log("delete mylist", myList)
        commit("setMylist", myList)
    } else {
        return false
    }
}
async function updateMyListItem(myList, id, commit) {
    const key = parseInt(id).toString()
    if (key in myList) {
        const pbxs = await loadPbxs(id)
        let info = await nftBriefInfo(id)
        info['pbxs'] = pbxs
        myList[key] = info
        commit("setMylist", myList)
    }
}
async function updateMkListItem(marketList, id, commit) {
    const key = parseInt(id).toString()
    if (key in marketList) {
        const market = await loadMarketinfo(id)
        let info = await nftBriefInfo(id)
        info['market'] = market
        marketList[key] = info
        getmkLists(marketList, commit)
    }
}

function startKeeper(_bsc, commit, _marketList, _myList) {
    copyObj(_bsc, bsc)
    marketList = _marketList
    myList = _myList
    if (bsc.ctrs.pbt.filters.Transfer) {
        bsc.ctrs.pbt.on(bsc.ctrs.pbt.filters.Transfer, async function (evt) {
            if (evt.event == "Transfer") {
                if (evt.args.to == bsc.addr) {
                    await addToMyList(myList, evt.args.tokenId, commit)
                }
                if (evt.args.to == bsc.ctrs.pbmarket.address) {
                    await addToMarketList(marketList, evt.args.tokenId, commit)
                }
                if (evt.args.from == bsc.addr) {
                    deleteFromMyList(myList, evt.args.tokenId, commit)
                }
                if (evt.args.from == bsc.ctrs.pbmarket.address) {
                    deleteFromMkList(marketList, evt.args.tokenId, commit)
                }
            }
        })
    }
    if (bsc.ctrs.pbpuzzlehash.filters.WithdrawPuzzleHashChanged) {
        bsc.ctrs.pbpuzzlehash.on(bsc.ctrs.pbpuzzlehash.filters.WithdrawPuzzleHashChanged, async function (evt) {
            if (evt.event == 'WithdrawPuzzleHashChanged') {
                console.log("WithdrawPuzzleHashChangedr", evt)
                await updateMyListItem(myList, evt.args.pbtId, commit)
            }
        })
    }
    if (bsc.ctrs.pbpuzzlehash.filters.DepositPuzzleHashChanged) {
        bsc.ctrs.pbpuzzlehash.on(bsc.ctrs.pbpuzzlehash.filters.DepositPuzzleHashChanged, async function (evt) {
            if (evt.event == 'DepositPuzzleHashChanged') {
                console.log("DepositPuzzleHashChanged", evt)
                await updateMyListItem(myList, evt.args.pbtId, commit)
            }
        })
    }
    if (bsc.ctrs.pbmarket.filters.OnSale) {
        bsc.ctrs.pbmarket.on(bsc.ctrs.pbmarket.filters.OnSale, async function (evt) {
            if (evt.event == "OnSale") {
                await updateMkListItem(marketList, evt.args.tokenId, commit)
            }
        })
    }
}

async function preloadOwnedPBTList(commit, _myList, cnt) {
    const addr = await bsc.signer.getAddress()
    console.log('checking ', addr)
    for (var i = 0; i < cnt; i++) {
        const idx = await bsc.ctrs.pbt.tokenOfOwnerByIndex(addr, i)
        const info = await nftBriefInfo(idx)
        _myList[idx.toString()] = info
        commit("setMylist", _myList)
    }
}

async function preload(commit, _myList) {
    const cnt = await bsc.ctrs.pbt.balanceOf(bsc.signer.getAddress())
    preloadOwnedPBTList(commit, _myList, cnt.toNumber()) // NO 'await' for faster return
    return cnt
}

exports.preload = preload
exports.startKeeper = startKeeper
exports.formatToken = formatToken
exports.parseToken = parseToken
exports.tokenSymbol = tokenSymbol
